#include<stdio.h>
#define Limit 100

int stackA[Limit], stackB[Limit], stackC[Limit];
int sizeA = 0, sizeB = 0, sizeC = 0;

void push(int *stack, int item, int *size) {

  if ((*size)>Limit) {
    printf("Stack Overflow.\n");
  }
  else {
    stack[*size] = item;
    (*size)++;
  }
}

int pop(int *stack, int *size) {

  if ((*size)<=0) {
    printf("Stack Underflow.\n");
  }
  else {
    int x = stack[(*size)-1];
    (*size)--;
    return x;
  }
}

void copy_stack() {

  int x;
  while(sizeA>0) {
    x = pop(stackA, &sizeA);
    push(stackB, x, &sizeB);
  }
  while(sizeB>0) {
    x = pop(stackB, &sizeB);
    int temp = x;
    push(stackC, x, &sizeC);
    push(stackA, temp, &sizeA);
  }
}

int main() {

  int size, iter, item;
  //clrscr();
  printf("# Project:    Copying Stack Contents      #");
  printf("\n# Architects: Mrinal Wahal & Ashwin Gupta #");
  printf("\n# Subject:    Data Structures Using C     #");
  printf("\n# Purpose:     Sem-End Open-Ended-Project #");
  printf("\n\nEnter Size of Source Stack: ");
  scanf("%d", &size);

  for (iter = 0; iter < size; iter ++) {
    printf("Enter Item: ");
    scanf("%d", &item);
    push(stackA, item, &sizeA);
  }
  copy_stack();

  printf("\nSource Stack: ");

  for (iter = 0; iter < size; iter++) {
    printf("%d", stackA[iter]);
  }

  printf("\nTemporary Stack: ");

  for (iter = 0; iter < size; iter++) {
    printf("%d", stackB[iter]);
  }

  printf("\nTarget Stack: ");
  for (iter = 0; iter < size; iter++) {
    printf("%d", stackC[iter]);
  }
  printf("\n");
  return 0;
}
